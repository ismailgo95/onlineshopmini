<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CI_system extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('url');

		$this->load->model('M_user');
		$this->load->model('M_buku');
		$this->load->model('M_customer');
		$this->load->model('M_keranjang');
		$this->load->model('M_transaksi');
		$this->load->model('M_detail_transaksi');
		
	}

	public function index($id=null)
	{
		
		$data['tampilbarang']=$this->M_buku->cari_buku("*","");
		
		$data['customer']=base_url('CI_system/daftar_customer');
		$data['url']=base_url('CI_system/aksilogin');

		$data['akses']=$this->session->userdata('akses');
		$data['email']=$this->session->userdata('email');
		$data['nama']=$this->session->userdata('nama');

		$data['log']=$this->session->userdata('log');
		// Untuk Menampilkan harga Jumlah Beli di header
		$datacustomer = $this->M_customer->data_customer("*", "where email='" . $data['email'] . "' ")->row_array(0);
		$data['notiftotal'] = $this->M_keranjang->cari_keranjang("sum(harga_beli*jumlah_beli) as total", "where id_customer='" . $datacustomer[ 'id_customer'] . "' ")->row_array(0);
		/////
		$this->load->view('template/v_header',$data);
		$this->load->view('template/v_konten');
		$this->load->view('template/v_footer');
	}
	public function cari($id = null)
	{
		if($id == null){
			$cari = "";
		}
		else{
			$cari = $id;
		}
		$data['tampilbarang'] = $this->M_buku->cari_buku("*", "WHERE namabuku like '%" . $cari . "%' ");
		$data['customer'] = base_url('CI_system/daftar_customer');
		$data['url'] = base_url('CI_system/aksilogin');

		$data['akses'] = $this->session->userdata('akses');
		$data['email'] = $this->session->userdata('email');
		$data['nama'] = $this->session->userdata('nama');

		$data['log'] = $this->session->userdata('log');
	
	
		$this->load->view('template/v_header', $data);
		$this->load->view('template/v_konten');
		$this->load->view('template/v_footer');
	}

	public function aksilogin()
	{
		$cari=$this->M_user->cari_user("*","Where email='$_POST[email]' and password='$_POST[password]'");
		if ($cari->num_rows()==1) {
			$res=$cari->row_array(0);
			if ($res['akses']=="Admin") {
				//cek Akses Jika Pelamar
				$carinamaadmin=$this->M_user->cari_user("*","where email='".$res['email']."'")->row_array(0);

				$this->session->set_userdata('log',$res['id']);
				$this->session->set_userdata('email',$res['email']);
				$this->session->set_userdata('akses',$res['akses']);
				$this->session->set_userdata('nama','Admin');
				header("location:".base_url("CI_system"));
			}
			if ($res['akses']=="Customer") {
				//cek Akses Jika Pelamar
				$carinamacustomer=$this->M_user->cari_user("*","where email='".$res['email']."'")->row_array(0);

				$this->session->set_userdata('log',$res['id']);
				$this->session->set_userdata('email',$res['email']);
				$this->session->set_userdata('akses',$res['akses']);
				$this->session->set_userdata('nama',$carinamacustomer['nama']);
				header("location:".base_url("CI_system"));
			}
		}else{
			echo "<script>alert('Email/Password Salah');document.location='".base_url('CI_system/v_header')."'</script>";
		}
	}

	public function keluar()
	{
		$this->session->sess_destroy();
		header("location:".base_url());
	}

	// CUSTOMER
	public function daftar_customer()
	{
		//ini Dari data User
		$this->M_user->email=$_POST["email"];
		$this->M_user->password=$_POST["password"];
		$this->M_user->akses="Customer";
		$this->M_user->tambah_ke_user();
		// ini dari data Pelamar
		$this->M_customer->nama=$_POST['nama'];
		$this->M_customer->hp=$_POST['nohp'];
		$this->M_customer->alamat=$_POST['alamat'];
		$this->M_customer->email=$_POST['email'];
		$this->M_customer->tambah_customer();

		echo "
		<script>
			alert('Customer Berhasil Registrasi');
			document.location='".base_url('CI_system/index')."'
		</script>
		";
	}
	public function data_customer()
	{
		$data['datacustomer']=$this->M_customer->data_customer("*","");

		$this->load->view('template/v_header');
		$this->load->view('customer/v_customer',$data);
		$this->load->view('template/v_footer');
	}
	public function hapus_customer($id=null)
	{
		$this->M_customer->hapus_customer($id);
		echo "
		<script>
			alert('Data Customer Berhasil dihapus');
			document.location='".base_url('CI_system/data_customer')."'
		</script>
		";
	}
	public function ubah_profil($id=null)
	{
		$data['id']=$id;
		$data['email']=$this->session->userdata('email');
		$data['datacustomer']=$this->M_customer->data_customer("*","where email='".$data['email']."' ")->row_array(0);
		$data['url']=base_url('CI_system/edit_profil/'.$data['datacustomer']['id_customer'].'');
		echo $id;
		$this->load->view('template/v_header');
		$this->load->view('customer/ubah_profil',$data);
		$this->load->view('template/v_footer');
	}
	public function edit_profil($id=null)
	{
		$this->M_customer->nama=$_POST['nama'];
		$this->M_customer->hp=$_POST['hp'];
		$this->M_customer->alamat=$_POST['alamat'];
		$this->M_customer->email=$_POST['email'];
		$this->M_customer->edit_profil($id);

		echo "
		<script>
			alert('Profil Berhasil Di ubah !');
			document.location='".base_url('CI_system/ubah_profil')."'
		</script>";
	}
	public function ubah_password()
	{

		$data['url']=base_url('CI_system/perbarui_password/');
		$this->load->view('template/v_header');
		$this->load->view('customer/ubah_password',$data);
		$this->load->view('template/v_footer');
	}
	public function perbarui_password()
	{
		$data['email']=$this->session->userdata('email');
		$data['datapassword']=$this->M_user->cari_user("*","where email='".$data['email']."' ")->row_array(0);

		if ($_POST['passbaru'] == $_POST['konfirmasipassword']) {
			if ($data['datapassword']['password'] == $_POST['passwordlama']){
				$this->M_user->passbaru=$_POST['passbaru'];
				$this->M_user->ubah_password($data['email']);
				echo "
				<script>
					alert('PasswordBerhasil Di ubah !');
					document.location='".base_url('CI_system/ubah_password')."'
				</script>";
			}else{
				echo "
					<script>
						alert('Password Lama Salah');
					</script>";
			}
		}else{
			echo "
				<script>
					alert('Password Baru tidak sama dengan Konfirmasi Password');
				</script>";
		}
	}

	// DATA BUKU
	public function data_buku($id=null)
	{
		$data['id']=$id;
		if ($id != null) {

		$data['ubah']=$this->M_buku->cari_buku("*","where id_buku='$id'")->row_array(0);
		$data['url']=base_url('CI_system/ubah_buku/'.$id.'');
		}
		else {
			$data['url']=base_url('CI_system/simpan_buku');

		}

		$data['hasilcover']=$this->M_buku->cari_buku("*","");

		$this->load->view('template/v_header');
		$this->load->view('buku/v_buku',$data);
		$this->load->view('template/v_footer');
	}

	public function simpan_buku()
	{
		$this->M_buku->kategori=$_POST['kategori'];
		$this->M_buku->nama_buku=$_POST['nama_buku'];
		$this->M_buku->penerbit=$_POST['penerbit'];
		$this->M_buku->tahun=$_POST['tahun'];
		$this->M_buku->pengarang=$_POST['pengarang'];
		$this->M_buku->harga=$_POST['harga'];
		$this->M_buku->stok=$_POST['stok'];
		$this->M_buku->deskripsi=$_POST['deskripsi'];
		$this->M_buku->tambah_buku();

		$config['file_name'] = 'buku_'.$this->db->insert_id();
		$config['allowed_types'] = 'jpg';
		$config['upload_path'] = 'upload';

		$config['overwrite'] = TRUE;

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		$data = '';
		if ($this->upload->do_upload('file')) {
			$data = $this->upload->data();
		}

		echo "
		<script>
			alert('Buku Berhasil Di tambah !');
			document.location='".base_url('CI_system/data_buku')."'
		</script>";
	}
	public function ubah_buku($id=null)
	{
		
		$this->M_buku->kategori=$_POST['kategori'];
		$this->M_buku->nama_buku=$_POST['nama_buku'];
		$this->M_buku->penerbit=$_POST['penerbit'];
		$this->M_buku->tahun=$_POST['tahun'];
		$this->M_buku->pengarang=$_POST['pengarang'];
		$this->M_buku->harga=$_POST['harga'];
		$this->M_buku->stok=$_POST['stok'];
		$this->M_buku->deskripsi=$_POST['deskripsi'];
		$this->M_buku->ubah_buku($id);

		$config['file_name'] = 'berita_'.$id;
		$config['allowed_types'] = 'jpg';
		$config['upload_path'] = 'upload';

		$config['overwrite'] = TRUE;

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		$data = '';
		if ($this->upload->do_upload('file')) {
			$data = $this->upload->data();
		}

		echo "
		<script>
			alert('Buku Berhasil diubah');
			document.location='".base_url('CI_system/data_buku')."'
		</script>
		";
	}
	public function detailbuku($id=null)
	{
		$data['id']=$id;	
		$data['akses']=$this->session->userdata('akses');
		$data['email']=$this->session->userdata('email');
		$data['nama']=$this->session->userdata('nama');
		$data['log']=$this->session->userdata('log');

		$data['detailbuku']=$this->M_buku->cari_buku("*","where id_buku='$id'")->row_array(0);
		$cekidcustomer= $this->M_customer->data_customer("*","where email='".$data['email']."'")->row_array(0);


		$data['tampilbarang']=$this->M_buku->cari_buku("*","");
		$data['customer']=base_url('CI_system/daftar_customer');
		$data['url']=base_url('CI_system/aksilogin');

		$data['barang']=base_url('CI_system/pesan/'.$id.'/'.$cekidcustomer['id_customer'].'/'.$data['detailbuku']['harga'].'');

	
		$this->load->view('template/v_header',$data);
		$this->load->view('buku/d_buku',$data);
		$this->load->view('template/v_footer');
	}
	public function pesan($idbuku=null, $idcustomer=null, $harga=null)
	{
		$data['idbuku']=$idbuku;
		$data['idcustomer']=$idcustomer;
		$data['harga']=$harga;

		$this->M_keranjang->id_buku=$data['idbuku'];
		$this->M_keranjang->idcustomer=$data['idcustomer'];
		$this->M_keranjang->harga=$data['harga'];
		$this->M_keranjang->jumlah_buku=$_POST['jumlah_buku'];
		$this->M_keranjang->tambah();

		echo "
		<script>
			alert('Buku Berhasil Masuk Keranjang Ayo di Cek !');
			document.location='".base_url('CI_system')."'
		</script>
		";
	}
	public function keranjang()
	{
		$data['akses'] = $this->session->userdata('akses');
		$data['email'] = $this->session->userdata('email');
		$data['nama'] = $this->session->userdata('nama');
		$data['cekcustomer'] = $this->M_customer->data_customer("*", "where email='" . $data['email'] . "'")->row_array(0);
		$data['hasilkeranjang']=$this->M_keranjang->cari_keranjang("*","t inner join tb_buku k on t.id_buku=k.id_buku where id_customer='".$data['cekcustomer']['id_customer']."'");
		$data['url'] = base_url('CI_system/chekout');
		$this->load->view('template/v_header');
		$this->load->view('buku/keranjang',$data);
		$this->load->view('template/v_footer');
	}
	public function hapus_keranjang($id=null)
	{
		
		$this->M_keranjang->hapus($id);
		echo "
		<script>
			alert('Belanjaan Berhasil dihapus dari keranjang !');
			document.location='".base_url('CI_system/keranjang')."'
		</script>
		";
	}
	public function checkout()
	{
		$data['email']=$this->session->userdata('email');
		
		$data['cekcustomer']=$this->M_customer->data_customer("*","where email='".$data['email']."'")->row_array(0);
		$cekisikeranjang=$this->M_keranjang->cari_keranjang("*","where id_customer='".$data['cekcustomer']['id_customer']."'");

		// simpan Transaksi
		$this->M_transaksi->idcustomer=$data['cekcustomer']['id_customer'];
		$this->M_transaksi->tgl=date('Y-m-d');
		$this->M_transaksi->alamat = $_POST['alamattujuan'];
		$this->M_transaksi->status_bayar='proses';
		$this->M_transaksi->status_kirim='proses';
		$this->M_transaksi->tambah_transaksi();
		
		// cek id_transaksi terakhir
		$lastid=$this->db->insert_id();
		// simpan ke detail transaksi
		foreach($cekisikeranjang->result_array() as $res)
		{
			$this->M_detail_transaksi->id_transaksi=$lastid;
			$this->M_detail_transaksi->id_buku=$res['id_buku'];
			$this->M_detail_transaksi->jumlah_beli=$res['jumlah_beli'];
			$this->M_detail_transaksi->harga_beli=$res['harga_beli'];
			$this->M_detail_transaksi->status='Belum Kirim';
			$this->M_detail_transaksi->tambah_detailtransaksi();
		}
		// Hapus isi Keranjang setelahnya
		$this->M_keranjang->hapus_keranjang($data['cekcustomer']['id_customer']);

		echo "
		<script>
			alert('Lanjut ke Checkout');
			document.location='".base_url('CI_system/riwayat')."'
		</script>
		";
		
	}

	public function riwayat()
	{
		$data['email']=$this->session->userdata('email');
		$data['checkout']=$this->M_transaksi->cari_transaksi("*","t inner join tb_customer c on t.id_customer=c.id_customer where email='".$data['email']."'");

		$this->load->view('template/v_header');
		$this->load->view('buku/checkout',$data);
		$this->load->view('template/v_footer');
	}
	public function detail_transaksi($id=null)
	{
		$data['id']=$id;

		$data['url']=base_url('CI_system/bukti_pembayaran/'.$id.'');

		$data['detail_transaksi']=$this->M_detail_transaksi->cari_detailtransaksi("*","d inner join tb_buku b on d.id_buku=b.id_buku where id_transaksi='".$id."'");
		
		$this->load->view('template/v_header');
		$this->load->view('buku/detail_transaksi',$data);
		$this->load->view('template/v_footer');
	}
	public function bukti_pembayaran($id=null)
	{
		$this->M_detail_transaksi->id_transaksi=$id;
		$this->M_detail_transaksi->status_bayar='Menunggu Verivikasi';
		$this->M_detail_transaksi->ubah($id);

		$config['file_name'] = 'bukti_'.$id;
		$config['allowed_types'] = 'jpg';
		$config['upload_path'] = 'upload';

		$config['overwrite'] = TRUE;

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		$data = '';
		if ($this->upload->do_upload('file')) {
			$data = $this->upload->data();
		}

		echo "
		<script>
			alert('Berhasil Upload Pembayaran, Silahkan Menunggu !');
			document.location='".base_url('CI_system/riwayat')."'
		</script>
		";
	}

	// KELOLA ADMIN
	public function verifikasi($id=null)
	{
		$data['id']=$id;
		$data['verivikasi']=$this->M_transaksi->cari_transaksi("*","t inner join tb_customer c on t.id_customer=c.id_customer");
		$data['detail_penjualan'] = $this->M_detail_transaksi->cari_detailtransaksi("*", "d inner join tb_buku b on d.id_buku=b.id_buku where id_transaksi='" . $id . "'");


		$this->load->view('template/v_header');
		$this->load->view('admin/verivikasi',$data);
		$this->load->view('template/v_footer');
	}
	public function valid($id=null)
	{
		$this->M_detail_transaksi->id_transaksi=$id;
		$this->M_detail_transaksi->status_bayar='Valid';
		$this->M_detail_transaksi->ubah($id);

		echo "
		<script>
			alert('Berhasil Memvalidasi');
			document.location='".base_url('CI_system/verifikasi')."'
		</script>
		";
	}
	public function tidak_valid($id=null)
	{
		$this->M_detail_transaksi->id_transaksi=$id;
		$this->M_detail_transaksi->status_bayar='Tidak Valid';
		$this->M_detail_transaksi->ubah($id);

		echo "
		<script>
			alert('Gagal Memvalidasi');
			document.location='".base_url('CI_system/verifikasi')."'
		</script>
		";
	}
	public function kirim($id=null)
	{
		$this->M_detail_transaksi->id_transaksi=$id;
		$this->M_detail_transaksi->status_kirim='Terkirim';
		$this->M_detail_transaksi->ubah_status($id);

		echo "
		<script>
			alert('Berhasil Terkirim');
			document.location='".base_url('CI_system/verifikasi')."'
		</script>
		";
	}
	public function tidak_kirim($id=null)
	{
		$this->M_detail_transaksi->id_transaksi=$id;
		$this->M_detail_transaksi->status_kirim='Tidak Terkirim';
		$this->M_detail_transaksi->ubah_status($id);

		echo "
		<script>
			alert('Tidak Terkirim');
			document.location='".base_url('CI_system/verifikasi')."'
		</script>
		";
	}
	public function barang_diterima($id=null)
	{
		$this->M_detail_transaksi->id_transaksi=$id;
		$this->M_detail_transaksi->status_kirim='Barang Berhasil diterima';
		$this->M_detail_transaksi->ubah_status($id);

		echo "
		<script>
			alert('Berhasil Diterima');
			document.location='".base_url('CI_system/riwayat')."'
		</script>
		";
	}
	public function laporanPenjualan($tgl1 =null, $tgl2 = null)
	{
		$data['tgl1']=$tgl1;
		$data['tgl2']=$tgl2;
		$data['laporanpenjualan'] = $this->M_transaksi->cari_transaksi( "t.id_transaksi ,nama ,tanggal ,status_bayar ,status_kirim,SUM(harga_beli*jumlah_beli) as total", " t INNER JOIN tb_detailtransaksi dt ON t.id_transaksi=dt.id_transaksi INNER JOIN tb_customer c ON t.id_customer=c.id_customer WHERE tanggal >= '".$tgl1. "' and tanggal <= '" . $tgl2 . "' and status_bayar = 'Valid'  GROUP BY t.id_transaksi,nama,tanggal,status_bayar,status_kirim");

		$this->load->view('template/v_header');
		$this->load->view('admin/laporanPenjualan',$data);
		$this->load->view('template/v_footer');
	}



}

