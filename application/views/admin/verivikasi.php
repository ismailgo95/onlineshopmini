<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>

<section class="section pb-0 section-components">
	<div class="container">
		<div class="row pt-4">
			<div class="col-md-12">
				<div class="card border-primary">
					<!-- Card Header -->
					<div class="card-header pt-2 pb-0">
						<h5 class="mail" align="center">Verivikasi Pembayaran</h5>
					</div>
					<!-- Card Body -->
					<div class="card-body">
						<div class="row">
							<div class="col-md-12 mt-3 mb-1">
								<table class="table table-striped" id="myTable">
									<thead>
										<tr>
											<th class="text-center">No Pembelian</th>
											<th class="text-center">Penerima</th>
											<th class="text-center">Tanggal</th>
											<th class="text-center">Alamat Tujuan</th>
											<th class="text-center">Status Bayar</th>
											<th class="text-center">Status Kirim</th>
											<th class="text-center">Aksi</th>
										</tr>
									</thead>
									<tbody>
										<?php $no = 1; ?>
										<?php foreach ($verivikasi->result_array() as $vk) :
											if ($vk['status_bayar'] == 'Menunggu Verivikasi') {
												$link1 = '<br>
									 				<small><a href="' . base_url('upload/bukti_' . $vk['id_transaksi'] . '.jpg') . '" target="blank" class="btn btn-sm btn-primary">lihat bukti</a>
									 				</small>

									 				<br>
									 				<small><a href="' . base_url('CI_system/valid/' . $vk['id_transaksi'] . '') . '" class="text-green">Valid</a></small>|

									 				<small><a href="' . base_url('CI_system/tidak_valid/' . $vk['id_transaksi'] . '') . '" class="text-danger">Tidak Valid</a></small>';
											} else {
												$link1 = '';
											}
											if ($vk['status_bayar'] == 'Valid') {
												if ($vk['status_kirim'] == 'Barang Berhasil diterima') {
													$link2 = '';
												} else
													$link2 = '<br>
									 				<small>
									 				<a href="' . base_url('CI_system/kirim/' . $vk['id_transaksi'] . '') . '" class="text-success">Kirim</a>|
									 				<a href="' . base_url('CI_system/tidak_kirim/' . $vk['id_transaksi'] . '') . '" class="text-danger">Tidak Kirim</a>
									 				</small>';
											} else {
												$link2 = '';
											}
											?>
											<tr>
												<td class="text-center"><?= $vk['id_transaksi']; ?></td>
												<td class="text-center"><?= $vk['nama']; ?></td>
												<td class="text-center"><?= $vk['tanggal']; ?></td>
												<td class="text-center"><?= $vk['alamat']; ?></td>


												<td class="text-center">
													<?= $vk['status_bayar']; ?>
													<?php echo $link1; ?>
												</td>

												<td class="text-center">
													<?= $vk['status_kirim']; ?>
													<?php echo $link2; ?>

												</td>

												<td class="text-center">
													<a href="<?= base_url('CI_system/verifikasi/' . $vk['id_transaksi'] . '') ?>" class="btn btn-sm btn-warning">Periksa</a>
												</td>
											</tr>
										<?php endforeach ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>

					<?php if ($id != null) : ?>
						<div class="card-header pt-2 pb-0">
							<h5 class="mail" align="center">Detail Penjualan / Cetak Penjualan</h5>
						</div>
						<div class="card-body">
							<div class="row justify-content-center">
								<a onclick="printDiv('printableArea')" class="btn btn-sm btn-primary" style="color:white"><i class="fas fa-print fa-2x" style="color:white"></i></a>

								<div class="col-md-12 mt-3 mb-1" id="printableArea">
									<table class="table table">
										<thead>
											<tr>
												<th class="text-center">ID Transaksi</th>
												<th class="text-center">Nama Buku</th>
												<th class="text-center">Jumlah Beli</th>
												<th class="text-center">Harga Beli</th>
											</tr>
										</thead>
										<tbody>
											<?php $total = 0; ?>
											<?php foreach ($detail_penjualan->result_array() as $dp) : ?>
												<?php $total = $total + $dp['harga_beli']; ?>
												<tr>
													<td class="text-center"><?= $dp['id_transaksi']; ?></td>
													<td class="text-center"><?= $dp['namabuku']; ?></td>
													<td class="text-center"><?= $dp['jumlah_beli']; ?></td>
													<td class="text-center">
														<h6><b>Rp.<?= number_format($dp['harga_beli'], 0, ',', '.'); ?>,-</b></h6>
													</td>
												<?php endforeach ?>
											</tr>
											<tr>
												<td colspan="2"></td>
												<td class="text-center"><b>TOTAL</b></td>
												<td class="text-center">
													<h5><b>Rp. <?= number_format($total, 0, ',', '.') ?>,-</b></h5>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					<?php endif; ?>
					<!-- Card Footer -->
					<div class="card-footer ">
					</div>




				</div>
			</div>
		</div>
	</div>
</section>

<script>
	function printDiv(printableArea) {
		var printContents = document.getElementById(printableArea).innerHTML;
		var originalContents = document.body.innerHTML;

		document.body.innerHTML = printContents;
		window.print();
		document.body.innerHTML = originalContents;
	}
</script>