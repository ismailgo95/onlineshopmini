<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<div class="container pt-5">
  <div class="row">
    <div class="col-md-6">
      <span>
        <h6>Lihat Dari Tanggal</h6>
      </span>
      <input class="form-control" type='date' id="tgl1">
    </div>
    <div class="col-md-6">
      <span>
        <h6>Sampai Tanggal</h6>
      </span>
      <input class="form-control" type="date" id="tgl2">
    </div>
  </div>
  <div class="row pt-3 ">
    <div class="col-md-12">
      <a onclick="lihat_laporan()" class="btn btn-sm btn-info"><i class="far fa-eye fa-2x"></i></a>
      <a onclick="printDiv('printableArea')" class="btn btn-sm btn-warning"><i class="fas fa-print fa-2x"></i></a>
    </div>
  </div>
</div>
<?php if ($tgl1 != null and $tgl2 != null) : ?>
  <section class="section pb-0 section-components" id="printableArea">
    <div class="container">
      <div class="row pt-1">
        <div class="col-md-12">
          <div class="card border-primary">
            <!-- Card Header -->
            <div class="card-header pt-2 pb-0">
              <h5 class="mail" align="center">Laporan Penjualan</h5>
            </div>
            <!-- Card Body -->
            <div class="card-body">
              <div class="row">
                <div class="col-md-12 mt-3 mb-1">
                  <table class="table table-striped" id="">
                    <thead>
                      <tr>
                        <th class="text-center">No Pembelian</th>
                        <th class="text-center">Penerima</th>
                        <th class="text-center">Tanggal</th>
                        <th class="text-center">Status Bayar</th>
                        <th class="text-center">Status Kirim</th>
                        <th class="text-center">Total</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $no = 1; ?>
                      <?php $ttl = 0; ?>
                      <?php foreach ($laporanpenjualan->result_array() as $lp) :

                        if ($lp['status_bayar'] == 'Menunggu Verivikasi') {
                          $link1 = '<br>
									 				<small><a href="' . base_url('upload/bukti_' . $lp['id_transaksi'] . '.jpg') . '" target="blank" class="btn btn-sm btn-primary">lihat bukti</a>
									 				</small>

									 				<br>
									 				<small><a href="' . base_url('CI_system/valid/' . $lp['id_transaksi'] . '') . '" class="text-green">Valid</a></small>|

									 				<small><a href="' . base_url('CI_system/tidak_valid/' . $lp['id_transaksi'] . '') . '" class="text-danger">Tidak Valid</a></small>';
                        } else {
                          $link1 = '';
                        }
                        if ($lp['status_bayar'] == 'Valid') {
                          if ($lp['status_kirim'] == 'Barang Berhasil diterima') {
                            $link2 = '';
                          } else
                            $link2 = '<br>
									 				<small>
									 				<a href="' . base_url('CI_system/kirim/' . $lp['id_transaksi'] . '') . '" class="text-success">Kirim</a>|
									 				<a href="' . base_url('CI_system/tidak_kirim/' . $lp['id_transaksi'] . '') . '" class="text-danger">Tidak Kirim</a>
									 				</small>';
                        } else {
                          $link2 = '';
                        }
                        ?>
                        <tr>
                          <td class="text-center"><?= $lp['id_transaksi']; ?></td>
                          <td class="text-center"><?= $lp['nama']; ?></td>
                          <td class="text-center"><?= $lp['tanggal']; ?></td>

                          <td class="text-center">
                            <?= $lp['status_bayar']; ?>
                          </td>

                          <td class="text-center">
                            <?= $lp['status_kirim']; ?>
                          </td>

                          <td class="text-center"><b>Rp. <?= number_format($lp['total'], 0, ',', '.'); ?></b></td>
                        </tr>

                        <?php $ttl =  $ttl + $lp['total'];
                      endforeach ?>
                      <tr style="font-size:15px">
                        <td colspan="5" class="text-right"><b>Total</b></td>
                        <td class="text-right">
                          <b>Rp. <?= number_format($ttl, 0, ',', '.'); ?>,-</b>
                        </td>
                        <td></td>

                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>

            <!-- Card Footer -->
            <div class="card-footer ">
            </div>

          </div>
        </div>
      </div>
    </div>
  </section>
<?php endif ?>

<script>
  function lihat_laporan() {
    document.location = '<?php echo base_url('CI_system/laporanPenjualan/') ?>' + document.getElementById('tgl1').value + '/' +
      document.getElementById('tgl2').value;
  }

  function printDiv(printableArea) {
    var printContents = document.getElementById(printableArea).innerHTML;
    var originalContents = document.body.innerHTML;

    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
  }
</script>