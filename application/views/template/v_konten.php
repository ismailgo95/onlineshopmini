<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!-- Konten -->

<!-- Ini Searching Buku -->
<section class="section pb-0 section-components">
  <div class="container">
    <div class="row">
      <div class="col-md-11">
        <div class="form-group">
          <div class="input-group input-group-alternative mb-4">
            <div class="input-group-prepend">
              <span class="input-group-text bg-secondary"><i class="fas fa-search"></i></span>
            </div>
            <input class="form-control bg-secondary" placeholder="Cari buku apa..?" type="text" id="kotak_pencarian">
          </div>
        </div>
      </div>
      <div class="col-md-1">
        <a onclick="cari()" class="btn btn-sm btn-primary"><i class="fas fa-search fa-2x" style="color:white"></i></i></a>
      </div>
    </div>
  </div>
</section>

<section class="section pt-3 section-components">
  <div class="container mb-2">
    <div class="row justify-content-center">
      <div class="col-lg-12">
        <div class="row mt-0">
          <?php $n = 1; ?>
          <?php foreach ($tampilbarang->result_array() as $tb) : ?>
            <div class="col-md-4 mt-2 pt-1">
              <div class="card border-primary shadow-sm">
                <h6 class="card-header pt-2 pb-2"><small class="text-muted">Kategori</small>
                  &raquo; <?= $tb['kategori']; ?>
                </h6>
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-4">
                      <?php echo "<img src='" . base_url("./upload/buku_" . $tb['id_buku'] . ".jpg") . "' class='shadow bg-light rounded' width='80px' >"; ?>
                    </div>
                    <div class="col-md-8">
                      <h6 class="card-title"><?= strlen($tb['namabuku']) > 20 ? substr($tb['namabuku'], 0, 20) . "..." : $tb['namabuku']; ?></h6>
                      <p class="card-text">
                        <small>
                          <ul type="circle" style="padding-left: 20px;">
                            <li>Pengarang : <?= $tb['pengarang'] ?></li>
                            <li>Penerbit : <?= $tb['penerbit'] ?></li>
                            <li>Tahun : <?= $tb['tahun'] ?></li>
                          </ul>
                        </small></p>
                    </div>
                  </div>
                  <hr class="mt-2">
                  <b class="text-danger"><small>Rp. </small><?= number_format($tb['harga'], 0, ',', '.') ?>,-</b>
                  <a href="<?= base_url('CI_system/detailbuku/' . $tb['id_buku'] . '') ?>" class="btn btn-sm btn-primary float-right"><i class="fa fa-cart-plus"></i> Detail...</a>
                </div>
              </div>
              <br>
            </div>
            <?php $n++; ?>
          <?php endforeach ?>
        </div>
      </div>
    </div>

  </div>
</section>

<script>
  function cari() {
    document.location = "<?= base_url('CI_system/cari/'); ?>" +
      document.getElementById("kotak_pencarian").value;
  }
</script>