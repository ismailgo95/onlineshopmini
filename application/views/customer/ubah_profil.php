<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="section">
  <div class="container">
    <div class="card card-profile shadow">
      <h5 align="center" class="mail pt-3">profil anda</h5>
      <div class="px-4">
        <div class="row justify-content-center">
          <div class="col-lg-6" align="center">
            <div class="card-profile-image">
              <a href="#">
                <img src="<?= base_url('') ?>./assets/img/brand/logo.png" class=" pt-4" width='200px'>
              </a>
            </div>
          </div>
          <div class="col-md-6">
            <div class="text-center mt-5">
             
              <h3><?php echo $datacustomer['nama'] ?></h3>
              <div class="h6 font-weight-300"><h6><?php echo $datacustomer['hp'] ?></h6></div>
              <div class="h6 mt-4"><h6><?php echo $datacustomer['alamat'] ?></h6></div>
              <div><h6><?php echo $datacustomer['email'] ?></h6></div>
            </div>

          </div>
        </div>
        <form action="<?php echo $url; ?>" method="post">
          <div class="mt-5 py-5 border-top text-center">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <input type="text" class="form-control" id="" placeholder="Masukan Nama" name="nama" value="<?php echo $datacustomer['nama'] ?>">
                </div>
                
                <div class="form-group">
                  <input type="text" class="form-control" id="" placeholder="Masukan Alamat" name="hp" value="<?php echo $datacustomer['hp'] ?>">
                </div>
              </div>
              <div class="col-md-6 justify-content-center">
                <div class="form-group">
                  <input type="text" class="form-control" id="" placeholder="Masukan Nomor Handphone" name="alamat" value="<?php echo $datacustomer['alamat'] ?>">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" id="" placeholder="Masukan Email" name="email" value="<?php echo $datacustomer['email'] ?>">
                </div>
              </div>
            </div>
            <div class="row justify-content-center pt-3">
              <div class="col-md-7">
                <button type="submit" class="btn btn-lg btn-success form-control ">Perbarui</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>