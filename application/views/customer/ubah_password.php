<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="section">
  <div class="container">
    <div class="card card-profile shadow">
      <h5 align="center" class="mail pt-3">Ubah Password</h5>
      <div class="px-4">
        <div class="row justify-content-center">
          <div class="col-lg-6" align="center">
            <div class="card-profile-image">
              <a href="#">
                <img src="<?= base_url('') ?>./assets/img/brand/logo.png" class=" pt-4" width='200px'>
              </a>
            </div>
          </div>
        </div>
        <form action="<?php echo $url; ?>" method="post" >
          <div class="mt-5 py-5 border-top text-center">
            <div class="row justify-content-center">
              <div class="col-md-6 ">
                <div class="form-group">
                  <input type="text" class="form-control" id="" placeholder="Password Lama" name="passwordlama" required>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" id="" placeholder="Password Baru" name="passbaru" required>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" id="" name="konfirmasipassword" placeholder="Confirm Password" required>
                </div>
              </div>
            </div>
            <div class="row justify-content-center pt-3">
              <div class="col-md-6">
                <button type="submit" class="btn btn-lg btn-success form-control ">Perbarui</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>