<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>

<section class="section pb-0 section-components">
	<div class="container">
		<div class="row pt-4">
			<div class="col-md-12">
				<div class="card border-primary">
					<!-- Card Header -->
					<div class="card-header pt-2 pb-0">
						<h5 class="mail" align="center">Checkout</h5>
					</div>
					<!-- Card Body -->
					<div class="card-body">
						<div class="row">
							<div class="col-md-12 mt-3 mb-1">
								<table class="table table-striped">
									<thead>
										<tr>
											<th class="text-center">No Pembelian</th>
											<th class="text-center">Penerima</th>
											<th class="text-center">Tanggal</th>
											<th class="text-center">Alamat Tujuan</th>
											<th class="text-center">Status Bayar</th>
											<th class="text-center">Status Kirim</th>
											<th class="text-center">Aksi</th>
										</tr>
									</thead>
									<tbody>
										<?php $no = 1; ?>
										<?php foreach ($checkout->result_array() as $ck) : ?>
											<?php if ($ck['status_kirim'] == 'Terkirim') {
												$link1 = '<small>
									 								<a href="' . base_url('CI_system/barang_diterima/' . $ck['id_transaksi'] . '') . '" class="btn btn-sm btn-success ">Barang Diterima</a>
									 								</small>';
											} else {
												$link1 = '';
											}
											?>

											<tr>
												<td class="text-center"><?= $ck['id_transaksi']; ?></td>
												<td class="text-center"><?= $ck['nama']; ?></td>
												<td class="text-center"><?= $ck['tanggal']; ?></td>
												<td class="text-center"><?= $ck['alamat_tujuan']; ?></td>
												<td class="text-center"><?= $ck['status_bayar']; ?></td>

												<td class="text-center">
													<?= $ck['status_kirim']; ?><br>
													<?= $link1; ?>
												</td>

												<td class="text-center">
													<a href="<?= base_url('CI_system/detail_transaksi/' . $ck['id_transaksi'] . '') ?>" class="btn btn-sm btn-warning">Periksa</a>
												</td>
											</tr>
										<?php endforeach ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<!-- Card Footer -->
					<div class="card-footer ">
					</div>

				</div>
			</div>
		</div>
	</div>
</section>