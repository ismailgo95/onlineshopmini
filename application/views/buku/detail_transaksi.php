<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="section pb-0 section-components">
	<form action="<?php echo $url; ?>" method="post" enctype="multipart/form-data">
	<div class="container">
		<div class="row pt-4">
			<div class="col-md-12">
				<div class="card border-primary">
					<!-- Card Header -->
					<div class="card-header pt-2 pb-0">
						<h5 class="mail" align="center">KONFIRMASI PEMBAYARAN ANDA </h5>
					</div>
					<!-- Card Body -->
					<div class="card-body">
						<div class="row">
							<div class="col-md-12 mt-3 mb-1">
								<table class="table table">
									<thead>
									 	<tr>
											<th class="text-center">ID Transaksi</th>
											<th class="text-center">Nama Buku</th>
											<th class="text-center">Jumlah Beli</th>
											<th class="text-center">Harga Beli</th>
										</tr>
									</thead> 
								 	<tbody>
								 		<?php $total=0; ?>
							 			<?php foreach($detail_transaksi->result_array() as $dt) : ?>
							 			<?php $total = $total + $dt['harga_beli']; ?>
													<tr>
														<td class="text-center"><?= $dt['id_transaksi'];?></td>
														<td class="text-center"><?= $dt['namabuku'];?></td>
														<td class="text-center"><?= $dt['jumlah_beli'];?></td>
														<td class="text-center"><h6><b>Rp.<?= number_format($dt['harga_beli'],0,',','.') ;?>,-</b></h6></td>
							 			<?php endforeach ?>
							 				</tr>
													<tr>
														<td colspan="2"></td>
														<td class="text-center"><b>TOTAL</b></td>
														<td class="text-center"><h5><b>Rp. <?= number_format($total,0,',','.') ?>,-</b></h5></td>
													</tr>
							 					<tr>
							 						<td colspan="4" class="text-center mail bg-secondary">UPLOAD BUKTI PEMBAYARAN</td>
							 					</tr>
							 					<tr>
														<td colspan="4" align="center">
                <img src="<?= base_url() ?>images/cover-kosong.png" id="tampil" name="tampil" class="shadow p-1  bg-light rounded" alt="" style="width: 170px;">
               </td>
													</tr>
													<tr>
														<td colspan="4" align="center">
															<input type="file" name="file" onchange="readURL(this);" class="form-control-sm" id="file" aria-describedby="cover" accept="image/gif, image/jpeg, image/png">
                &nbsp;&nbsp;<i><small id="cover" class="form-text text-muted mt-0">**Ukuran file cover Maks. 10 MB;.</small></i>
														</td>
													</tr>
								 	</tbody>
								</table>
								<button type="submit" name="simpan" class="btn btn-md btn-success form-control" ><i class="fa fa-save fa-2x"><span class="mail">&nbsp;Upload</span></i></button>
							</div>
						</div>
					</div>
					<!-- Card Footer -->
					<div class="card-footer ">
					</div>

				</div>
			</div>
		</div>
	</div>
	</form>
</section>