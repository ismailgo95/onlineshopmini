<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>

<section class="section pb-0 section-components">
	<div class="container">
		<div class="row pt-4">
			<div class="col-md-12">
				<div class="card border-primary">
					<!-- Card Header -->
					<div class="card-header pt-2 pb-0">
						<h5 class="mail" align="center">Review Keranjang</h5>
					</div>
					<!-- Card Body -->
					<div class="card-body">
						<div class="row">
							<div class="col-md-12 mt-3 mb-1">
								<table class="table table-striped">
									<thead>
										<tr>
											<th class="text-center">No.</th>
											<th class="text-center">Buku</th>
											<th class="text-center">Harga</th>
											<th class="text-right">Jumlah</th>
											<th class="text-right">Subtotal</th>
											<th class="text-center">Aksi</th>
										</tr>
									</thead>
									<tbody>

										<?php $no = 1; ?>
										<?php $total = 0; ?>
										<?php foreach ($hasilkeranjang->result_array() as $keranjang) : ?>
											<?php $subtotal = $keranjang['harga_beli'] * $keranjang['jumlah_beli'];  ?>
											<tr>
												<td class="text-center"><?= $no++ ?></td>
												<td class="text-center"><?= $keranjang['namabuku'] ?></td>
												<td class="text-center"><b>Rp. <?= number_format($keranjang['harga_beli'], 0, ',', '.')  ?>,-</b></td>
												<td class="text-right"><?= $keranjang['jumlah_beli'] ?></td>
												<td class="text-right"><b>Rp. <?= number_format($subtotal, 0, ',', '.'); ?>,-</b></td>
												<td align="center">
													<a href="<?= base_url('CI_system/hapus_keranjang/' . $keranjang['id_keranjang'] . '') ?>" class=""><i class="fas fa-window-close fa-2x text-danger"></i></a>
												</td>
											</tr>

											<?php $total =  $total + $subtotal; ?>
										<?php endforeach ?>
										<tr>
											<td colspan="4" class="text-right"><b>Total</b></td>
											<td class="text-right">
												<h5><b>Rp. <?= number_format($total, 0, ',', '.'); ?>,-</b></h5>
											</td>
											<td></td>
										</tr>
										<tr>

											<td>
												<form action="<?= base_url('CI_system/checkout') ?>" method="post">
													<input type="text" name="alamattujuan" class="form-control" value="<?php echo $cekcustomer['alamat'] ?>">
											</td>

										</tr>
										<tr>
											<td class="text-right" colspan="6">
												<button type="submit" name="checkout" class="btn btn-lg btn-success"><i class="fas fa-shopping-bag"></i>&nbsp;Checkout</button>
											</td>
										</tr>
										</form>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<!-- Card Footer -->
					<div class="card-footer ">
					</div>

				</div>
			</div>
		</div>
	</div>
</section>