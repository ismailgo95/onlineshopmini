<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_detail_transaksi extends CI_model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function tambah_detailtransaksi()
	{
		return $this->db->insert('tb_detailtransaksi',array(
			'id_transaksi'=> $this->id_transaksi,
			'id_buku'=> $this->id_buku,
			'jumlah_beli'=> $this->jumlah_beli,
			'harga_beli'=> $this->harga_beli
			));
	}
	public function ubah($id)
	{
		$this->db->where('id_transaksi',$id);
		return $this->db->update('tb_transaksi',array(
			'status_bayar'=> $this->status_bayar
			));
	}
	public function ubah_status($id)
	{
		$this->db->where('id_transaksi',$id);
		return $this->db->update('tb_transaksi',array(
			'status_kirim'=> $this->status_kirim
			));
	}
	
	public function cari_detailtransaksi($kolom, $kriteria)
	{
		//Pencarian
		// return $this->db->query("select * from berita");
		return $this->db->query("select $kolom from tb_detailtransaksi $kriteria");
	}
}